https://www.youtube.com/watch?v=aysgHRmzG3w&feature=youtu.be&utm_source=mautic&utm_medium=newsletter&utm_campaign=aviso_goweek

npm install mysql --save-dev
adonis migration:run

nvm use v9.6.1 && adonis serve --dev



# Adonis API application

This is the boilerplate for creating an API server in AdonisJs, it comes pre-configured with.

1. Bodyparser
2. Authentication
3. CORS
4. Lucid ORM
5. Migrations and seeds

## Setup

Use the adonis command to install the blueprint

```bash
adonis new yardstick --api-only
```

or manually clone the repo and then run `npm install`.


### Migrations

Run the following command to run startup migrations.

```js
adonis migration:run
```
